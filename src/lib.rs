pub enum CollectionType {
    Application,
    Logical,
    NamedArray,
    Other,
    Physical,
    Report,
    UsageModifier,
    UsageSwitch
}

pub struct Collection {
    pub id: u32,
    pub typ: CollectionType,
    pub usage_id: u16,
    pub usage_page: u16,
}

pub enum ReportType {
    Input,
    Output,
    Feature
}

pub trait BooleanControlDesc {
    fn id(&self) -> u32;
    fn absolute(&self) -> bool;

    fn report_id(&self) -> u16;
    fn report_type(&self) -> ReportType;

    fn usage_id(&self) -> u16;
    fn usage_page(&self) -> u16;

    fn parent_collections(&self) -> Vec<Collection>;
}

pub trait BooleanControl {
    type Desc: BooleanControlDesc;

    fn id(&self) -> u16;
    fn usage_id(&self) -> u16;
    fn usage_page(&self) -> u16;

    fn get_active(&self) -> bool;
    fn set_active(&mut self, active: bool);

    fn get_descriptor(&self) -> Self::Desc;
}

pub trait NumericControlDesc {
    fn has_null(&self) -> bool;
    fn id(&self) -> u32;
    fn absolute(&self) -> bool;

    fn logical_max(&self) -> i32;
    fn logical_min(&self) -> i32;
    fn physical_max(&self) -> i32;
    fn physical_min(&self) -> i32;

    fn report_count(&self) -> u32;
    fn report_id(&self) -> u16;
    fn report_size(&self) -> u32;
    fn report_type(&self) -> ReportType;

    fn unit(&self) -> u32;
    fn unit_exponent(&self) -> u32;

    fn usage_id(&self) -> u16;
    fn usage_page(&self) -> u16;

    fn parent_collections(&self) -> Vec<Collection>;
}

pub trait NumericControl {
    type Desc: NumericControlDesc;

    fn id(&self) -> u32;
    fn usage_id(&self) -> u16;
    fn usage_page(&self) -> u16;

    fn is_grouped(&self) -> bool;

    fn get_scaled_value(&self) -> i64;
    fn set_scaled_value(&mut self, val: i64);

    fn get_value(&self) -> i64;
    fn set_value(&mut self, val: i64);

    fn get_descriptor(&self) -> Self::Desc;
}

pub trait FeatureReport {
    type B: BooleanControlDesc;
    type BC: BooleanControl<Desc=Self::B>;
    type N: NumericControlDesc;
    type NC: NumericControl<Desc=Self::N>;

    fn data(&mut self) -> &mut Vec<u8>;
    fn id(&self) -> u16;

    fn boolean_control(&self, desc: Self::B) -> Self::BC;
    fn numeric_control(&self, desc: Self::N) -> Self::NC;
}

pub trait InputReport {
    type B: BooleanControlDesc;
    type BC: BooleanControl<Desc=Self::B>;
    type N: NumericControlDesc;
    type NC: NumericControl<Desc=Self::N>;

    fn data(&mut self) -> &mut Vec<u8>;
    fn id(&self) -> u16;
    fn activated(&self) -> Vec<Self::BC>;
    fn transitioned(&self) -> Vec<Self::BC>;

    fn boolean_control(&self, desc: Self::B) -> Self::BC;
    fn numeric_control(&self, desc: Self::N) -> Self::NC;
}

pub trait OutputReport {
    type B: BooleanControlDesc;
    type BC: BooleanControl<Desc=Self::B>;
    type N: NumericControlDesc;
    type NC: NumericControl<Desc=Self::N>;

    fn data(&mut self) -> &mut Vec<u8>;
    fn id(&self) -> u16;

    fn boolean_control(&self, desc: Self::B) -> Self::BC;
    fn numeric_control(&self, desc: Self::N) -> Self::NC;
}

/// A HID device
pub trait Device {
    type BCD: BooleanControlDesc;
    type NCD: NumericControlDesc;
    type F: FeatureReport<B=Self::BCD, N=Self::NCD>;
    type O: OutputReport<B=Self::BCD, N=Self::NCD>;
    
    fn vendor_id(&self) -> u16;
    fn product_id(&self) -> u16;
    fn version(&self) -> u16;
    fn usage_page(&self) -> u16;
    
    fn default_feature_report(&self) -> Self::F;
    fn make_feature_report(&self, id: u16) -> Self::F;

    fn default_output_report(&self) -> Self::O;
    fn create_output_report(&self, id: u16) -> Self::O;

    fn boolean_controls(&self, report_type: ReportType, usage_page: u16, usage_id: u16) -> Vec<Self::BCD>;
    fn numeric_controls(&self, report_type: ReportType, usage_page: u16, usage_id: u16) -> Vec<Self::NCD>;

    fn device_selector(&self, usage_page: u16, usage_id: u16) -> Vec<u8>;
    fn device_selector2(&self, usage_page: u16, usage_id: u16, vendor: u16, product: u16) -> Vec<u8>;
    
    // TODO: replace these with Future
    fn send_feature_report(&self, report: Self::F) -> Result<(), usize>;
    fn send_output_report(&self, report: Self::O) -> Result<(), usize>;
    fn get_input_report(&self) -> Option<Self::O>;
}

pub trait DeviceDescriptor {
    fn vendor_id(&self) -> u32;
    fn product_id(&self) -> u32;
    fn path(&self) -> Option<&[u8]>;
    fn serial_number(&self) -> Option<&[u8]>;
    fn manufacturer(&self) -> &[u8];
    fn product_name(&self) -> &[u8];
    fn release(&self) -> u16;
    //fn interface(&self) -> u16;
    // hidapi exposes this, it's not clear to me what it's useful for.
}

/// Allows listing and opening HID devices on a system.
pub trait Manager {
    type D: Device;
    type DD: DeviceDescriptor;
    type E: Iterator<Item=Self::DD>;
    fn new() -> Self;
    fn enumerate_all(&mut self) -> Option<Self::E>;
    fn open(&mut self, desc: Self::DD) -> Self::D;
}

#[cfg(target_os = "linux")]
#[path = "linux.rs"]
pub mod iface;