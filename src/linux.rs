extern crate libudev;

use std::os::unix::ffi::OsStrExt;
use std::os::unix::io::RawFd;

pub struct DeviceDescriptor {
    vendor: u32,
    product: u32,
    path: Option<Vec<u8>>,
    serial_number: Option<Vec<u8>>,
    mfgr: Vec<u8>,
    prod_name: Vec<u8>,
    release: u16,
    //interface: u16,
}

impl ::DeviceDescriptor for DeviceDescriptor {
    fn vendor_id(&self) -> u32 { self.vendor }
    fn product_id(&self) -> u32 { self.product }
    fn path(&self) -> Option<&[u8]> { self.path.map(|x| x.as_slice()) }
    fn serial_number(&self) -> Option<&[u8]> { self.serial_number.map(|x| x.as_slice()) }
    fn manufacturer(&self) -> &[u8] { &self.mfgr }
    fn product_name(&self) -> &[u8] { &self.prod_name }
    fn release(&self) -> u16 { self.release }
}

pub struct DeviceIterator<'a>(libudev::Devices<'a>);

const BUS_USB: u16 = 3;
const BUS_BLUETOOTH: u16 = 5;

impl<'a> Iterator for DeviceIterator<'a> {
    type Item = DeviceDescriptor;

    fn next(&mut self) -> Option<DeviceDescriptor> {
        // this could stand to be refactored
        while let Some(d) =  self.0.next() {
            let hidp = match d.parent_with_subsystem_devtype("hid", None) {
                Ok(Some(d)) => d,
                _ => continue,
            };

            // try and parse out type/vendor/product from HID_ID
            let (typ, vendor, product) = match hidp.property_value("HID_ID").and_then(|x| x.to_str()) {
                Some(hidid) => {
                    let components = hidid.split(':');
                    let typ = components.next().and_then(|x| x.parse::<u16>().ok());
                    let vendor = components.next().and_then(|x| x.parse::<u32>().ok());
                    let product = components.next().and_then(|x| x.parse::<u32>().ok());
                    match (typ, vendor, product) {
                        (Some(typ), Some(vendor), Some(product)) => {
                            (typ, vendor, product)
                        },
                        _ => continue,
                    }
                },
                None => continue,
            };

            let prod_name = match hidp.property_value("HID_NAME").map(|x| x.as_bytes().to_vec()) {
                Some(p) => p,
                None => continue,
            };
            let serial_number = match hidp.property_value("HID_UNIQ").map(|x| x.as_bytes().to_vec()) {
                Some(p) => p,
                None => continue,
            };

            if typ == BUS_USB {
                let usbp = match d.parent_with_subsystem_devtype("usb", Some("usb_device")) {
                    Ok(Some(d)) => d,
                    _ => continue,
                };
                let manu = match usbp.attribute_value("manufacturer").map(|x| x.as_bytes().to_vec()) {
                    Some(m) => m,
                    None => continue,
                };
                let prod = match usbp.attribute_value("product").map(|x| x.as_bytes().to_vec()) {
                    Some(m) => m,
                    None => continue,
                };
                let relno = match usbp.attribute_value("bcdDevice").and_then(|x| x.to_str().and_then(|x| x.parse::<u16>().ok())) {
                    Some(r) => r,
                    None => panic!("this shouldn't be reachable, please file a bug report with the device information"),
                };
                return Some(DeviceDescriptor {
                    vendor: vendor,
                    product: product,
                    path: d.devnode().map(|x| x.as_os_str().as_bytes().to_vec()),
                    serial_number: Some(serial_number),
                    mfgr: manu,
                    prod_name: prod,
                    release: relno,
                });
            } else if typ == BUS_BLUETOOTH {
                return Some(DeviceDescriptor {
                    vendor: vendor,
                    product: product,
                    path: d.devnode().map(|x| x.as_os_str().as_bytes().to_vec()),
                    serial_number: None,
                    mfgr: vec![],
                    prod_name: prod_name,
                    release: 0,
                });
            } else {
                continue;
            }
        }
        None
    }
}

pub struct Manager<'a> {
    ctx: libudev::Context,
    _ph: ::std::marker::PhantomData<libudev::Device<'a>>,
}

pub struct Device {
    rawfd: RawFd,
}

impl ::Device for Device {
    type BCD = BooleanControlDesc;
    type NCD = NumericControlDesc;
    type F = FeatureReport;
    type O = OutputReport;
    
    fn vendor_id(&self) -> u16 { unimplemented!() }
    fn product_id(&self) -> u16 { unimplemented!() }
    fn version(&self) -> u16 { unimplemented!() }
    fn usage_page(&self) -> u16 { unimplemented!() }
    
    fn default_feature_report(&self) -> Self::F { unimplemented!() }
    fn make_feature_report(&self, id: u16) -> Self::F { unimplemented!() }

    fn default_output_report(&self) -> Self::O { unimplemented!() }
    fn create_output_report(&self, id: u16) -> Self::O { unimplemented!() }

    fn boolean_controls(&self, report_type: ReportType, usage_page: u16, usage_id: u16) -> Vec<Self::BCD> { unimplemented!() }
    fn numeric_controls(&self, report_type: ReportType, usage_page: u16, usage_id: u16) -> Vec<Self::NCD> { unimplemented!() }
    fn device_selector(&self, usage_page: u16, usage_id: u16) -> Vec<u8> { unimplemented!() }
    fn device_selector2(&self, usage_page: u16, usage_id: u16, vendor: u16, product: u16) -> Vec<u8> { unimplemented!() }
    
    // TODO: replace these with Future
    fn send_feature_report(&self, report: Self::F) -> Result<(), usize> { unimplemented!() }
    fn send_output_report(&self, report: Self::O) -> Result<(), usize> { unimplemented!() }
    fn get_input_report(&self) -> Option<Self::O> { unimplemented!() }
}

impl<'a> ::Manager for Manager<'a> {
    type D = Device;
    type DD = DeviceDescriptor;
    type E = DeviceIterator<'a>;

    fn new() -> Manager<'a> {
        Manager {
            ctx: libudev::Context::new(),
            _ph: ::std::marker::PhantomData,
        }
    }

    fn enumerate_all(&mut self) -> Option<DeviceIterator<'a>> {
        match libudev::Enumerator::new(self.ctx) {
            Ok(e) => {
                match e.match_subsystem("hidraw").and_then(libudev::Enumerator::scan_devices) {
                    Ok(ds) => {
                        Some(DeviceIterator(ds))
                    },
                    Err(_) => None,
                }
            }
            Err(_) => None,
        }
    }

    fn open(&mut self, desc: DeviceDescriptor) -> Device {
        unimplemented!()
    }
}