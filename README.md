# lodapi

Pure-Rust library for talking to devices with the HID protocol. See below for OS
support matrix.

## Supported operating systems

- Linux (`hidraw` interface): ❌
- Windows: ❌
- macOS: ❌
- Redox: ❌
- Robigalia: ❌

Anything not listed is unsupported, and I have no plans for adding support for
it. If you wish to add support for an unlisted platform, I'd be very happy to
accept your pull requests. However, please see Robigalia's [contribution
guide](https://robigalia.org/contributing.html). In particular, note the
requirements on commit signing and accepting the DCO.

## License

lodapi is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or
   http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in Serde by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
